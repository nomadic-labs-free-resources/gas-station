import { IEstimates } from './interfaces/Estimates';
import { Bytes, Key, MichelineType, Nat, Option, Or, pair_to_mich, Signature, string_to_mich } from '@completium/archetype-ts-types'
import { InMemorySigner } from "@taquito/signer"
import { hex2buf } from "@taquito/utils"
import { ContractAbstraction, ContractProvider, MichelCodecPacker, OpKind, TezosToolkit } from "@taquito/taquito"
import blake from "blakejs"
import { GsnError } from '../Helpers';

export class StationToolkit {


    // Separate step submission
    separateSubmit = async (
        toolkit,
        contractAddress,
        signerKey,
        signature,
        paramsHash,
        entrypoint,
        params
    ) => {
        const contract = await toolkit.contract.at(contractAddress)

        const batch = toolkit
            .batch()
            .withContractCall(contract.methods.permits(signerKey, signature, paramsHash))
            .withContractCall(contract.methods[entrypoint](...params))

        const batchOp = await batch.send()
        return batchOp
    }

    // One step submission
    OneSubmit = async (
        toolkit,
        contractAddress,
        signerKey,
        signature,
        entrypoint,
        params
    ) => {
        const contract = await toolkit.contract.at(contractAddress)
        const tx = contract.methods[entrypoint](...params, signerKey, signature)
        const op = await tx.send()
        return op
    }

    permitParamHash = async function (
        toolkit,
        contract, //: ContractAbstraction<ContractProvider>,
        entrypoint, //: string,
        parameters //: any
    ) {
        const wrapped_param: any = contract.methods[entrypoint](
            ...parameters
        ).toTransferParams().parameter?.value;
        const wrapped_param_type = contract.entrypoints.entrypoints[entrypoint];
        const raw_packed = await toolkit.rpc.packData({
            data: wrapped_param,
            type: wrapped_param_type,
        });

        return blake.blake2bHex(hex2buf(raw_packed.packed), undefined, 32)
    }

    // Check tx hash and validate it
    validateTxHash = async (
        toolkit: TezosToolkit,
        contractAddress: string,
        entrypoint: string,
        params: any[],
        paramHash: string
    ) => {
        const contract = await toolkit.contract.at(contractAddress)

        const calculatedHash = await this.permitParamHash(
            toolkit,
            contract,
            entrypoint,
            params
        )

        if (calculatedHash != paramHash) {
            throw new GsnError("hash_does_not_match_to_params", {
                calculated: calculatedHash,
                input: paramHash,
            })
        }
    }

    // Init tezos taquito toolkit
    initToolkit = (rpcEndpoint, secretKey) => {
        const toolkit = new TezosToolkit(rpcEndpoint)
        toolkit.setProvider({
            signer: new InMemorySigner(secretKey),
        })

        toolkit.setPackerProvider(new MichelCodecPacker())
        return toolkit
    }

    // batch estimation
    estimateAsBatch = async (toolkit: TezosToolkit, txs: any[]) =>
        toolkit.estimate.batch(
            txs.map((tParams: any) => ({ kind: OpKind.TRANSACTION, ...tParams }))
        )

    // estimate txs fees
    estimate = async (toolkit: TezosToolkit, permitParams: any): Promise<IEstimates[]> => {
        const { signature, hash, pubkey, contractAddress, callParams } = permitParams

        const { entrypoint, params } = callParams

        const contract = await toolkit.contract.at(contractAddress)


        let estim: IEstimates[] = []

        // // if (contract.methods.hasOwnProperty("permit")) {  // a revoir
        const permit = contract.methods
            .permit(pubkey, signature, hash)
            .toTransferParams({})


        const userEntrypoint = contract.methods[entrypoint](...params).toTransferParams(
            {}
        )

        await toolkit.estimate.transfer(permit).then((est: IEstimates) => {
            const estObj = {
                gasLimit: est.gasLimit,
                minimalFeeMutez: est.minimalFeeMutez,
                storageLimit: est.storageLimit,
                suggestedFeeMutez: est.suggestedFeeMutez,
                totalCost: est.totalCost,
                usingBaseFeeMutez: est.usingBaseFeeMutez,
                burnFeeMutez: est.burnFeeMutez
            }
            estim.push(estObj)
        })
            .catch((error) => { return error });
        await toolkit.estimate.transfer(userEntrypoint).then((est: IEstimates) => { // just for testing
            const estObj = {
                gasLimit: est.gasLimit,
                minimalFeeMutez: est.minimalFeeMutez,
                storageLimit: est.storageLimit,
                suggestedFeeMutez: est.suggestedFeeMutez,
                totalCost: est.totalCost,
                usingBaseFeeMutez: est.usingBaseFeeMutez,
                burnFeeMutez: est.burnFeeMutez
            }
            estim.push(estObj)
        })
            .catch((error) => { return error });

        // const estimatesTest = await this.estimateAsBatch(toolkit, [userEntrypoint, userEntrypoint])
        // console.log({ estimatesTest });

        return estim
    }

}