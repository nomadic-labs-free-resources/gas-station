import { GsnError } from './../Helpers/helper';
import { toolkit } from './server';
import { Request, Response, Router } from 'express';
import { StationToolkit } from './Gtool';

export class StationController {

    toolkit: StationToolkit;

    constructor(toolkit: StationToolkit) {
        this.toolkit = toolkit;
    }

    // estimate txs fees
    async estimateTx(req: Request, res: Response) {
        try {
            const estimates = await this.toolkit.estimate(toolkit, req.body)
            res.json({ "Permit Tx Estimation": estimates[0], "User Tx Estimation": estimates[1] })
        } catch (error) {
            this.errorHandler(error, req, res)
        }
    }

    // separate submission submit
    async separateSubmitTx(req: Request, res: Response) {
        const { signature, hash, pubkey, contractAddress, callParams } = req.body
        try {
            await this.toolkit.validateTxHash(
                toolkit,
                contractAddress,
                callParams.entrypoint,
                callParams.params,
                hash
            )
            // For a potential use of an oracle for payment with other tokens
            // const estimates = await this.toolkit.estimate(toolkit, req.body) 
            // let gasEstimate = estimates[0].suggestedFeeMutez + estimates[1].suggestedFeeMutez
            const operation = await this.toolkit.separateSubmit(
                toolkit,
                contractAddress,
                pubkey,
                signature,
                hash,
                callParams.entrypoint,
                callParams.params
            )
            res.json({
                hash: operation.hash,
                results: operation.results,
            })
        } catch (error) {
            this.errorHandler(error, req, res)
            return;
        }

    }

    // one submission submit
    async oneSubmitTx(req: Request, res: Response) {
        const { signature, hash, pubkey, contractAddress, callParams } = req.body
        try {
            await this.toolkit.validateTxHash(
                toolkit,
                contractAddress,
                callParams.entrypoint,
                callParams.params,
                hash
            )
            // For a potential use of an oracle for payment with other tokens
            // const estimates = await this.toolkit.estimate(toolkit, req.body) 
            // let gasEstimate = estimates[0].suggestedFeeMutez + estimates[1].suggestedFeeMutez
            const operation = await this.toolkit.OneSubmit(
                toolkit,
                contractAddress,
                pubkey,
                signature,
                callParams.entrypoint,
                callParams.params
            )
            res.json({
                hash: operation.hash,
                results: operation.results,
            })
        } catch (error) {
            this.errorHandler(error, req, res)
            return;
        }

    }

    // get server status
    async getStatus(req: Request, res: Response) {
        res.json({ status: 'ok' });
    }

    errorHandler(err, _, res) {
        if (err instanceof GsnError) {
            return res.status(400).json({
                error: err.message,
                ...err.data,
            })
        }
        res.status(400).json({
            error: err.name,
            id: err.id,
            message: err.message,
        })
    }

    buildRoutes(): Router {
        const router = Router();
        // router.use(this.errorHandler.bind(this));
        router.get('/getServerStatus', this.getStatus.bind(this));
        router.post('/separateSend', this.separateSubmitTx.bind(this));
        router.post('/oneSend', this.oneSubmitTx.bind(this));
        router.post('/estimate', this.estimateTx.bind(this));
        return router;
    }
}