import { GsnError } from './../Helpers/helper';
import { pinoHttp } from 'pino-http';
import {  StationToolkit, StationController } from './index';
import express from 'express';
import helmet from "helmet";
import errorHandler from "errorhandler"
import cors from "cors";
import * as dotenv from 'dotenv'
import { prodErrorHandler } from '../Helpers';
dotenv.config()


const port = process.env.PORT || 3009;


const gtool = new StationToolkit();
const stationController = new StationController(gtool);

const app = express()
  .use(helmet())
  .use(cors())
  .use(express.json())
  .use(express.urlencoded({ extended: true }))
  .use("/station", stationController.buildRoutes())
  .use(
    process.env.NODE_ENV === "development"
      ? errorHandler({ log: true })
      : prodErrorHandler
  )


export const toolkit = gtool.initToolkit(process.env.GHOSTNET_RPC_PROVIDER, process.env.SECRET_KEY)


app.listen(port, () => {
  console.log(`Listening on port ${port}`);
})
