export interface IEstimates {

    burnFeeMutez: number,
    gasLimit: number,
    minimalFeeMutez: number,
    storageLimit: number,
    suggestedFeeMutez: number,
    totalCost: number,
    usingBaseFeeMutez: number;

}

