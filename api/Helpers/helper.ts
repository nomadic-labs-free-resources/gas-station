import { pinoHttp } from "pino-http";
import { InMemorySigner } from "@taquito/signer"
import express from "express"
import http from "http"


export class GsnError extends Error {
  data: Record<any, any>
  constructor(message, data) {
    super(message)
    this.name = "GsnError"
    this.data = data
  }
}

export const prodErrorHandler: express.ErrorRequestHandler = (
  err,
  _req,
  res,
  _next // eslint-disable-line
) => {
  console.log("prod encountered error", err)

  const code = err.code || err.status || 500
  const codeMessage = http.STATUS_CODES[code]

  res.statusCode = code
  res.end(
    code === 500 && process.env.NODE_ENV === "production"
      ? codeMessage
      : (err.length && err) || err.message || codeMessage
  )
}
