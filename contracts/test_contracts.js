const {
    deploy,
    getAccount,
    getAddress,
    getContract
} = require('@completium/completium-cli');

const alice  = getAccount('alice');
const bob  = getAccount('bob');

const test = async () => {
    
    try {
        //Here the owner is set to alice, you can replace it by your address to be the owner of the contract
        const [permit, _] = await deploy('fa2permits.arl', {parameters: {owner: alice.pkh}});
    } catch(e) { console.log(e) }
    
  }
  test()