# Gas Station

This is a repository with a prototype of a gas station network on the Tezos blockchain. 
It uses a FA2 contract example that implements permits in its structure. This prototype allows you to perform transfer (and only transfer) remotely.

The goal of a gas station in blockchain, is to allow a user to execute a transaction without paying the network fees associated to this transaction.
This gas station works with an API on which the user will send the transactions that he wants to make. The API then sends it to a relayer that will execute it on chain.

## Disclaimer

This repository is only a prototype of a gas station working on a specific architecture of smart contract and it can't usually be duplicated and applied as it is now to your projects. You should have to make at least some modifications for it to be compatible with your smart contracts.

The structure used here is a single contract with the permit interface inside it. They're not stored in another contract like he can be done in the archetype repository for example (link in annex). The way API is working is by submitting the permit then perform the transfer that will consume the stored permit. It's the two step method (detailed in the Documentation/Tzip 17). You can also do the one step method by only changing a parameter in a file located in the frontend folder (find the way to do so in the README of the frontend folder).
So there is other ways to do so (having two contracts). If you want to implement them, you'll have to modify this repo.

## Getting started

### Requirements

- npm
- node

### Run

Set up the API:

```shell
cd api
npm i
npm start
```

Set up the front-end:
(To interact with it, you can find the JSON in the README inside the frontend folder)

```shell
cd frontend
npm i
npm run dev
```

Deploy the contract used to test (copy the address gotten):

```shell
cd contracts
npm i @completium/completium-cli
node test_contracts.js
```

## Documentation

### Introduction

This document presents the specifications and architecture of the gas station prototype. This project is a continuation, and inspired by the Tezos gas station network project of Madfish.

It aims to collect transactions from users and send them to the network via a relayer that is not the user. That means that the user will not pay for the fees and will not directly interact with the chain. To do so, it will use permits (tzip 17). A user gives the authorization, by signing the parameters of the transaction he wants to perform, to someone to act in his place. It is therefore necessary that the contract on which the transaction will take place implements the tzip 17. Otherwise there is no way to perform any action from another address than yours.

There are actually two ways to implement permits, one is to have the permit structure directly in your contract or to have it in another contract, usually named permit, that will store the permits. In this case, your first contract will call the permit contract to consume them. 
Here we used the first case, the structure of our work is made with a FA2 contract that has a direct storage for permits. The front-end and API are made according to it. If your contracts are made in the other way, you’ll have to modify our work especially the API but the logic will remain similar.

### Tzip 17 (permits)

The Tzip 17 released in 2020, proposes a standard for a pre-sign or "Permit" interface: a lightweight on-chain emulation of tz accounts (tz1, tz2). To submit a transaction and pay for the fees associated with it, you usually need an account but with this standard you can give the authorization to another account to perform actions through transactions in your name. 
Permits are using some key parameters that are mandatory. The transaction parameters (address from and to in the case of a transfer, amount, …), own address of the contract (KT), the chain id and a counter that is unique and that will be incremented permit after permit. You then need to sign all this. When submitting the permit, this signed data as well as the previous transaction parameters hashed with black2b (a cryptographic hash function) need to be sent to the contract. The created permit is associated with an expiration date, by default every contract that implements the tzip 17 has a default expiration value chosen by the creator, but you can change this value. 
The standard is composed of some entry points that the contracts will need to implement in order to use the permits. One of submission and one to set the expiry of the permits. There are actually two possibilities to the submission entry point. It can be done in one step, by submitting both permit (signature and address of the signer) and parameters to the same entry point in one transaction, or in separate steps, by submitting the permit itself and after in a second transaction, the parameters of the transaction.

### Architecture plan

![Screenshot](/png/plan.png)

### Description of the architecture

**Front-end:**

For the front-end, we used React, one of its libraries (Mantine) and Tailwind (a css framework). 

This front is just an example with this kind of API (a single FA2 contract that implements permits in its architecture). If you choose to implement another type of architecture for your contract and the way to interact with permits (with the permits stored on another contract for example), you will have to modify the front-end and its structure in order to get the correct data for your transactions.

Here is the looks that our front-end for this prototype has:

![Screenshot](/png/front.png)

In this gas station prototype, the user that wants to use it will go to our front-end (a local website for now) on which he’ll be able to connect his tezos wallet. He will first enter the address of the contract that he wants to interact with. For now he needs to enter it because it’s for testing and if you want to test it with your own contract you can do so by just entering the address. In the end with your own app, the address will be known and the user will not have to do this step. 
After, he needs to select the entry point that he wants to use for example transfer in case of an FA2 (the entrypoint selected must be one that he can interact with, some have requirements like being the owner). Then enters all the parameters of the transaction (from, to, amount, …) he wants to perform. In our testing scenario with our contract that implements the permits itself, a call is made to itself to register the permit for you and then another one to the transfer entry point that will consume the signed permit. If the structure of the contract is different and you need to submit the permit in the same transaction (using the permit_transfer entry point), you’ll be able to do so by changing the way the frontend interacts with the API (find the way in the README of the frontend folder). In order for it to make one transaction instead of two.

Once everything is well filled, the user will click on the sign button and will sign the structured data according to the tzip 17 model in his wallet extension. 
All those data (parameters, signature) will then be sent to the API that will store them.


**API:**

For the API part, we used expressjs to communicate between the front and the part that will interact with the chain.

The API is composed of a tezos account that will push the transactions after extracting data from the json onto the network with interaction libraries like taquito. The tezos account will be the one who pays the network fees in tez. He will call the wanted contracts on the wanted entry points with the permit of the user. If the parameters are well entered by the user and that they match the network state (for example for a transfer if the user has an amount >= in his balance), the transaction should be performed on the network. 

To change the relayer you can do so by changing the private key in the .env file inside the api folder. Be aware that if you put your key and that you push the repo online, a bot could drain all your assets from your account.

## Annex 

- TZIP-17 spec on Tezos Agora: 
https://tzip.tezosagora.org/proposal/tzip-17/

- Example of a repo with contracts that implement permits (fa2_fungible.arl):
https://github.com/completium/archetype-fa2

- Gas station project of Madfish:
https://github.com/madfish-solutions/tezos-gsn