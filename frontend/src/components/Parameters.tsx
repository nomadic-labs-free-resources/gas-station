import React, { FC, useState } from "react";
import { Stack, Group, Button } from "@mantine/core";
import { ITypeDef, StationContextType } from "../context/station.type";
import TypeDef from "./TypeDef";

interface Props {
  entryPointsParams: ITypeDef[];
}

export const Parameters: FC<Props> = (props) => {
  
  return (
    <>
      <div className=" mt-5 bg-white py-10 px-4  w-full border-gray-300 rounded-lg ">
        <Stack dir="column" align="center" spacing={4}>
          <TypeDef typedef={props.entryPointsParams} first="parameter" />
        </Stack>
      </div>
    </>
  );
};

export default Parameters;
