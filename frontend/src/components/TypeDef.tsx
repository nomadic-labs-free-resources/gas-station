import React from "react";
import { Arg, ITypeDef } from "../context/station.type";

interface TypeDefProps {
  typedef: ITypeDef[];
  first: string;
}

const TypeDef: React.FC<TypeDefProps> = ({ typedef, first }) => {
  const highlightType = (expr: string): string => {
    return expr.replace(/(\$\w+)/g, '<span class="accent--text">$1</span>');
  };
  console.log("TypeDef => ", typedef);
  
  // if (typedef.length > 0) {
  //   if ("args" in typedef[0]) {
  //     console.log("Type checking => ", typedef[0].args.length);
  //   } else {
  //     console.log("No args");
  //   }
  // }

  return (
    <div className="d-flex flex-column parameters text-[20px]">
        <>
          {typedef.map((def: ITypeDef, i: number) => (
            <div key={i} className="mb-2">
              {i === 0 ? (
                <span className="font-weight-light">{first}&nbsp;</span>
              ) : def.name ? (
                <span className="font-weight-light">{def.name}&nbsp;</span>
              ) : null }
              <span
                className="tree--text text-blue-600"
                dangerouslySetInnerHTML={{ __html: highlightType(def.type) }}
              />
              {"args" in def ? (
                <>
                  {def.args.map((arg: Arg, j: number) => (
                    <div key={i + j} className="pl-4">
                      <span>{arg.key}&nbsp;</span>
                      <span
                        className="tree--text text-blue-600"
                        dangerouslySetInnerHTML={{
                          __html: highlightType(arg.value),
                        }}
                      />
                    </div>
                  ))}
                </>
              ) : (
                ""
              )}
            </div>
          ))}
        </>
    </div>
  );
};

export default TypeDef;
