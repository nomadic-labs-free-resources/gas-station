import React, { FC, useState } from "react";
import {
  TextInput,
  Textarea,
  Title,
  Loader,
  ColorInput,
  Flex,
  NativeSelect,
  Stack,
  Group,
  Button,
  Box,
  JsonInput,
  Notification,
} from "@mantine/core";
import { useForm } from "@mantine/form";
import { UseForm } from "@mantine/form/lib/types";
import { validateContractAddress } from "@taquito/utils";
import { StationContext } from "../context/StationContext";
import { ITypeDef, StationContextType } from "../context/station.type";
import { useValidatedState } from "@mantine/hooks";
import Parameters from "./Parameters";
import { IconCheck, IconX } from "@tabler/icons-react";

interface Props {
  entryNames: string[];
}

interface Contract {
  address: string;
  entrypoint: string;
  // params : any[];
}

export const Forms: FC<Props> = (props) => {
  const {
    getContractEntrypoints,
    getEntrypointsParams,
    entryParams,
    jsonArea,
    sign,
    entryNames,
    notification,
    setNotification,
    btnLoading,
    setJsonArea,
  } = React.useContext(StationContext) as StationContextType;
  const [{ value, lastValidValue, valid }, setContractValue] =
    useValidatedState("", (val) => validateContractAddress(val) === 3, true);
  const [selectedValue, setSelectedValue] = useState("");
  // const [json, setJson] = useState("");

  const form = useForm({
    initialValues: {
      contract: {
        address: "",
        entrypoint: "",
      },
    },
    validate: {
      contract: {
        address: (value: string) =>
          validateContractAddress(value) === 3 ? null : "Invalid address",
        entrypoint: (value: string) =>
          value.length > 0 ? null : "Permit entrypoint Required",
      },
    },
    validateInputOnChange: true,
  });

  return (
    <>
      <center>
        {notification.type ? (
          <>
            {notification.type.includes("Error") ? (
              <Notification
                icon={<IconX size="1.1rem" />}
                color="red"
                mt={20}
                w="50%"
                title="Error"
                onClick={() => {
                  setNotification({ type: "", message: "" });
                }}
              >
                {notification.message}
              </Notification>
            ) : (
              <Notification
                icon={<IconCheck size="1.1rem" />}
                color="teal"
                mt={20}
                w="50%"
                title="Success"
                onClick={() => {
                  setNotification({ type: "", message: "" });
                }}
              >
                {notification.message}
              </Notification>
            )}
          </>
        ) : (
          ""
        )}
      </center>
      <Flex dir="row" justify="space-between" sx={{ width: "120%" }} mt={10}>
        <Box sx={{ width: "100%" }}>
          <div className=" mt-5 bg-white py-10 px-20 mr-4 border-gray-300 rounded-lg ">
            <form
              onSubmit={form.onSubmit(
                (values, _event) => {
                  getEntrypointsParams(value, selectedValue);
                },
                (validationErrors, _values, _event) => {
                  console.log("Values => ", _values);
                  console.log(validationErrors);
                }
              )}
            >
              <Stack px={40} dir="column" align="center" spacing={2}>
                <Flex
                  dir="row"
                  justify="space-between"
                  sx={{ width: "120%" }}
                  mt={10}
                >
                  <br />
                  <TextInput
                    sx={{ width: "100%" }}
                    label="Contract Address"
                    color="white"
                    text-color="white"
                    placeholder="kt1..."
                    {...form.getInputProps("adrs")}
                    value={value}
                    error={!valid}
                    required
                    mr={20}
                    onChange={(event) => {
                      setContractValue(event.currentTarget.value);
                      getContractEntrypoints(event.currentTarget.value);
                      setJsonArea(jsonArea);
                    }}
                  />
                  <NativeSelect
                    sx={{ width: "50%" }}
                    data={
                      props.entryNames.length > 0
                        ? ["Select entrypoint", ...props.entryNames]
                        : ["entrypoint"]
                    }
                    label="Entrypoint"
                    required
                    withAsterisk
                    disabled={props.entryNames.length == 0 ? true : false}
                    onChange={(event) => {
                      setSelectedValue(event.currentTarget.value);
                      getEntrypointsParams(value, event.currentTarget.value);
                      setJsonArea(jsonArea);
                    }}
                  />
                </Flex>
                {entryParams.length > 0 ? (
                  <>
                    <Flex
                      dir="row"
                      justify="space-between"
                      sx={{ width: "120%" }}
                      mt={10}
                    >
                      <Textarea
                        sx={{ width: "100%" }}
                        label="Your json input parameters without permit (key and signature)"
                        placeholder="Inputs"
                        onChange={(event) => {
                          setJsonArea(event.currentTarget.value);
                        }}
                        value={jsonArea}
                        autosize
                      />
                    </Flex>
                    <Button
                      type="submit"
                      sx={{ width: "50%" }}
                      onClick={() => {
                        if (
                          value.length > 0 &&
                          selectedValue.length > 0 &&
                          jsonArea.length > 0 &&
                          valid &&
                          entryParams.length > 0
                        ) {
                          sign(value, selectedValue, jsonArea);
                        }
                      }}
                      className="mt-5  px-6 font-poppins font-medium text-[18px] text-white menu-transition mx-4 cursor-pointer  bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500  rounded-lg cursor-pointer hover:bg-[#2546bd] mt-5"
                    >
                      {!btnLoading ? (
                        "Sign"
                      ) : (
                        <Loader size={20} color="white" />
                      )}
                    </Button>
                  </>
                ) : (
                  ""
                )}
                {/* <Group position="right" mt="md">
                  <Button type="submit">Submit</Button>
                </Group> */}
              </Stack>
            </form>
          </div>
        </Box>
        <Box sx={{ width: "1%" }}></Box>
        {entryParams.length > 0 ? (
          <Box sx={{ width: "50%" }}>
            <Parameters entryPointsParams={entryParams} />
          </Box>
        ) : (
          ""
        )}
      </Flex>
    </>
  );
};

export default Forms;
