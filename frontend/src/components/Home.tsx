import React, { useState } from "react";
import styles from "../styles/style";
import { note } from "../assets";
import { StationContext } from "../context/StationContext";
import { StationContextType } from "../context/station.type";
import { TezosToolkit } from "@taquito/taquito";
import { BeaconWallet } from "@taquito/beacon-wallet";
import Forms from "./Forms";
import { Notification, Title } from "@mantine/core";
import Parameters from "./Parameters";
import { IconX } from "@tabler/icons-react";

const Home: React.FC = () => {
  const {
    connect,
    pkh,
    getContractEntrypoints,
    entryNames,
    test,
    notification,
  } = React.useContext(StationContext) as StationContextType;

  test();

  return (
    <section
      id="home"
      className={`flex md:flex-row flex-col ${styles.paddingY}`}
    >
      <div
        className={`flex-1 ${styles.flexCenter} flex-col xl:px-0 sm:px-16 px-6 mt-20`}
      >
        <div className="flex flex-row items-center py-[6px] px-4 bg-[red] rounded-[10px] mb-2">
          <img src={note} alt="discount" className="w-[32px] h-[32px]" />
          <p className={`${styles.paragraph} ml-2`}>
            This application has been created for the sole purpose of testing
            the gas station.
          </p>
        </div>
        <div
          className={`${styles.flexCenter} justify-center items-center w-full`}
        >
          <h1 className="font-poppins font-semibold ss:text-[72px] text-[52px] text-white ss:leading-[100.8px] leading-[75px]">
            Tezos Gas <span className={`text-gradient`}>Station</span>{" "}
          </h1>
        </div>
        <p
          className={`${styles.paragraph} justify-center items-center max-w-[470px] mt-5`}
        >
          Put the ghostnet address of your smart contract and test your contract
          without paying any fees. <br className="sm:block hidden" />{" "}
          <a
            target="_blank"
            href="https://tzip.tezosagora.org/proposal/tzip-17/#submission"
          >
            <span className={`${styles.flexCenter} text-gradient`}>
              Note that to test this app*, your contract must implement the Tzip
              17 standard with separate steps submission. You don't need to
              register permit. Just test your permit_entrypoint.
            </span>{" "}
          </a>
        </p>
        <button
          type="button"
          onClick={async () => {
            console.log("Key => ", pkh);
            connect();
            console.log("entryNames => ", entryNames);
          }}
          className={` mt-5 py-4 px-6 font-poppins font-medium text-[18px] text-white menu-transition mx-4 cursor-pointer  bg-gradient-to-r from-indigo-500 via-purple-500 to-pink-500  rounded-lg cursor-pointer hover:bg-[#2546bd]`}
        >
          {!pkh ? "Connect Wallet" : pkh.slice(0, 6) + "..." + pkh.slice(-6)}
        </button>
        <div className=" mt-10 ">
          {pkh ? (
            <>
              <p className="text-white text-center text-[20px]">
                Contract Informations
              </p>
              <Forms entryNames={entryNames} />
            </>
          ) : (
            ""
          )}
        </div>
        <p
          className={`${styles.paragraph} justify-center items-center max-w-[470px] mt-10`}
        >
          <a
            target="_blank"
            href="https://gitlab.com/nomadic-labs-support-team/support/gasstation"
          >
            <span className={`${styles.flexCenter} text-gradient`}>
              Feel free to re-implement this app to support one step or other
              features if you want.
            </span>{" "}
          </a>
        </p>
      </div>
    </section>
  );
};

export default Home;
