import styles from "./styles/style";
import { Home } from "./components";
import StationProvider from './context/StationContext'


function App() {
  return (
    <div className="bg-primary w-full overflow-hidden">
      <div className={`${styles.paddingX} ${styles.flexCenter}`}>
        <div className={`${styles.boxWidth}`}>
          <Home />
        </div>
      </div>
      {/* <div className={`bg-primary ${styles.paddingX} ${styles.flexCenter}`}>
        <div className={`${styles.boxWidth}`}>
          <Products />
          <Stats />
          <Subsproducts />
          <Feedback />
          <CTA />
          <Footer/>
          <Business />
        <Billing />
        <CardDeal />
        <Testimonials />
        <Clients />
        <CTA />
        <Footer />
        </div>
      </div> */}
    </div>
  );
}

export default App;