import { BeaconWallet } from "@taquito/beacon-wallet";
import { MichelCodecPacker, TezosToolkit } from "@taquito/taquito";
import * as React from "react";
import {
  EntrypoinType,
  ITypeDef,
  Notif,
  StationContextType,
  StationProviderProps,
} from "./station.type";
import { InMemorySigner } from "@taquito/signer";
import { buf2hex, hex2buf, validateContractAddress } from "@taquito/utils";
import { RpcClient } from "@taquito/rpc";
import { Schema } from "@taquito/michelson-encoder";
import { packDataBytes } from "@taquito/michel-codec";
import blake from "blakejs";
import { SigningType } from "@airgap/beacon-types";
import { Notification } from "@mantine/core";
import { IconCheck, IconX } from "@tabler/icons-react";
import Swal from "sweetalert2";

// import swal from 'sweetalert'

export const StationContext = React.createContext<StationContextType | null>(
  null
);

let rpc = "https://rpc.tzkt.io/ghostnet";

const StationProvider = ({ children }: StationProviderProps) => {
  const [btnLoading, setBtnLoading] = React.useState(false);
  const [pkh, setPkh] = React.useState("");
  const [entrypoints, setEntrypoints] = React.useState<EntrypoinType[]>([]);
  const [entryNames, setEntryNames] = React.useState<string[]>([]);
  const [entryParams, setEntryType] = React.useState<ITypeDef[]>([]);
  const [jsonArea, setJsonArea] = React.useState<string>("");
  const [notification, setNotification] = React.useState<Notif>({} as Notif);

  const Tezos = new TezosToolkit(rpc);
  const wallet = new BeaconWallet({ name: "Beacon Docs Taquito" });
  const client = new RpcClient(rpc);

  const connect = async () => {
    const activeAccount = await wallet.client.getActiveAccount();
    if (activeAccount) {
      console.log("Pk =>", activeAccount);
      console.log("Already connected:", activeAccount.address);
      setPkh(activeAccount.address);
      return activeAccount;
    } else {
      Tezos.setWalletProvider(wallet);
      try {
        console.log("Requesting permissions...");
        const permissions = await wallet.client.requestPermissions();
        console.log("Got permissions:", permissions.address);
        setPkh(permissions.address);
      } catch (error) {
        console.log("Got error:", error);
      }
    }
  };

  const getContractEntrypoints = async (contractAddress: string) => {
    if (validateContractAddress(contractAddress) !== 3) {
      setEntryNames([]);
      setJsonArea("");
      return;
    }
    var requestOptions: RequestInit = {
      method: "GET",
      redirect: "follow",
    };
    fetch(
      `https://api.ghostnet.tzkt.io/v1/contracts/${contractAddress}/entrypoints/`,
      requestOptions
    )
      .then((response) => response.text())
      .then((result) => {
        saveEntrypointName(JSON.parse(result));
      })
      .catch((error) => console.log("error", error));
  };

  const getEntrypointsParams = async (
    contractAddress: string,
    entrypoint: string
  ) => {
    if (validateContractAddress(contractAddress) !== 3) {
      setEntryNames([]);
      setEntryType([]);
      setJsonArea("");
      return;
    }
    if (entrypoint === "Select entrypoint") {
      setEntryType([]);
      setJsonArea("");
      setEntryType([]);
      return;
    }
    var requestOptions: RequestInit = {
      method: "GET",
      redirect: "follow",
    };
    fetch(
      `https://api.better-call.dev/v1/contract/ghostnet/${contractAddress}/entrypoints/schema?entrypoint=${entrypoint}`,
      requestOptions
    )
      .then((response) => response.text())
      .then((result) => {
        let resp = JSON.parse(result);
        setEntryType(resp.typedef);
        // console.log("Entry params =>", JSON.parse(resp));
      })
      .catch((error) => console.log("error", error));
    const entrypoints = await client.getEntrypoints(contractAddress);

    const storageSchema = new Schema(entrypoints.entrypoints[entrypoint]);
    const extractSchema = storageSchema.ExtractSchema();

    if (typeof extractSchema === "string") {
      let format = `{ "${extractSchema}" : "" }`;
      setJsonArea(format);
      return;
    }
    const newJson = JSON.stringify(extractSchema, null, 3);
    setJsonArea(newJson);
  };

  const saveEntrypointName = (entrypoints: EntrypoinType[]) => {
    let newEntrypoints: string[] = [];
    for (let index = 0; index < entrypoints.length; index++) {
      newEntrypoints.push(entrypoints[index].name);
    }
    setEntryNames(newEntrypoints);
  };

  const sign = async (
    contractAddress: string,
    entryPoint: string,
    jsonParams: any
  ) => {
    setNotification({ type: "", message: "" });
    if (validateContractAddress(contractAddress) !== 3) {
      setEntryNames([]);
      return;
    }
    setBtnLoading(true);
    try {
      const contract: any = await Tezos.contract.at(contractAddress);
      let values = Object.values(JSON.parse(jsonParams));

      const wrapped_param: any = contract.methods[entryPoint](
        ...values
      ).toTransferParams().parameter?.value;
      const wrapped_param_type = contract.entrypoints.entrypoints[entryPoint];
      const raw_packed = await Tezos.rpc.packData({
        data: wrapped_param,
        type: wrapped_param_type,
      });
      const param_hash = blake.blake2bHex(
        hex2buf(raw_packed.packed),
        undefined,
        32
      );
      let signature = await create_bytes_to_sign(
        Tezos,
        contractAddress,
        param_hash,
        contract
      );
      await callStationApi(
        contractAddress,
        entryPoint,
        values,
        param_hash,
        signature.toString()
      );
      setBtnLoading(false);
    } catch (error: any) {
      let notif: Notif = {
        type: "Error",
        message: error.message,
      };
      setNotification(notif);
      setBtnLoading(false);
    }
  };

  // call Api
  const callStationApi = async (
    contractAddress: string,
    entryPoint: string,
    params: any,
    param_hash: string,
    signature: string
  ) => {
    const activeAccount = await wallet.client.getActiveAccount();
    let formatStationData = {
      pubkey: activeAccount?.publicKey,
      signature: signature,
      hash: param_hash,
      contractAddress: contractAddress,
      callParams: {
        entrypoint: entryPoint,
        params: params,
      },
    };
    
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");
    var requestOptions: RequestInit = {
      method: "POST",
      headers: myHeaders,
      body: JSON.stringify(formatStationData),
      redirect: "follow",
    };
    fetch("http://127.0.0.1:3956/station/separateSend", requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log("result => ", result);
        let resp = JSON.parse(result);
        if (resp.error) {
          Swal.fire({
            icon: "error",
            title: resp.error,
            text: resp.message,
          });
        } else {
          Swal.fire({
            icon: "success",
            title: "Operation success",
            text: "Operation hash : " + resp.hash,
          });
        }
      })
      .catch((error) => {
        Swal.fire({
          icon: "error",
          title: "API_CALL_ERROR",
          text: "Something went wrong with the API call",
        });
      });
    //  console.log("formatStationData => ", formatStationData);
  };

  const test = async () => {
    const ca = "KT1QsE2r1ufPLMxWx8nH6rvhDFqyXN1HUcxg";
    // const client = new RpcClient("https://rpc.tzkt.io/ghostnet");
    // const entrypoints = await client.getEntrypoints(ca);

    // const storageSchema = new Schema(entrypoints.entrypoints["transfer"]);
    // const extractSchema = storageSchema.generateSchema();
    // console.log("Extract schema =>", extractSchema);

    // const newJson = JSON.stringify(extractSchema, null, 3);
    // console.log("newJson =>", newJson);

    // const contract = await Tezos.contract.at(ca);

    // const wrapped_param: any =
    //   contract.methods["burn"](42).toTransferParams().parameter?.value;
    // console.log("wrapped_param =>", wrapped_param);
    // const wrapped_param_type = contract.entrypoints.entrypoints["burn"];
    // console.log("wrapped_param_type =>", wrapped_param_type);
    // const raw_packed = await Tezos.rpc.packData({
    //   data: wrapped_param,
    //   type: wrapped_param_type,
    // });
    // console.log("raw_packed =>", raw_packed.packed);
    // let packed_param = raw_packed.packed;
    // const param_hash = blake.blake2bHex(hex2buf(packed_param), undefined, 32);
    // console.log("param_hash =>", param_hash);

    // console.log("param_hash =>", param_hash);
    // create_bytes_to_sign(Tezos, ca, param_hash);
  };

  const create_bytes_to_sign = async (
    Tezos: TezosToolkit,
    contractAddress: string,
    methodHash: string,
    contract: any
  ) => {
    const chainId = await Tezos.rpc.getChainId();

    const contractStorage: any = await contract.storage();
    let counter = 0;
    if (contractStorage.hasOwnProperty("permit")) {
      let adr = await wallet.client.getActiveAccount();
      try {
        const permits = await contractStorage.permit.get(
          adr?.address.toString()
        );
        counter = permits.counter.toNumber();
      } catch (error) {
        console.log("Has permit but not for this address");
      }
    } else {
      counter = 0;
    }
    console.log("There is Counter =>", counter);

    const sigParamData: any = {
      prim: "Pair",
      args: [
        {
          prim: "Pair",
          args: [
            {
              string: contractAddress,
            },
            {
              string: chainId,
            },
          ],
        },
        {
          prim: "Pair",
          args: [
            {
              int: counter,
            },
            {
              bytes: methodHash,
            },
          ],
        },
      ],
    };
    const sigParamType: any = {
      prim: "pair",
      args: [
        {
          prim: "pair",
          args: [
            { prim: "address" },
            {
              prim: "chain_id",
            },
          ],
        },
        {
          prim: "pair",
          args: [{ prim: "nat" }, { prim: "bytes" }],
        },
      ],
    };
    const sigParamPacked = packDataBytes(sigParamData, sigParamType);
    console.log("sigParamPacked =>", sigParamPacked.bytes);

    // signs the hash
    const signature: any = await wallet.client.requestSignPayload({
      signingType: SigningType.MICHELINE,
      payload: sigParamPacked.bytes,
    });
    // const signature = await Tezos.signer.sign(sigParamPacked.bytes);
    console.log("signature =>", signature);

    return signature.signature;
  };

  React.useEffect(() => {
    async function check() {
      const activeAccount = await wallet.client.getActiveAccount();
      if (activeAccount) {
        console.log("Already connected:", activeAccount.address);
        setPkh(activeAccount.address);
      }
    }
    check();
  }, []);

  return (
    <StationContext.Provider
      value={{
        test,
        notification,
        setNotification,
        btnLoading,
        jsonArea,
        sign,
        setJsonArea,
        entryNames,
        entryParams,
        pkh,
        connect,
        getContractEntrypoints,
        getEntrypointsParams,
      }}
    >
      {children}
    </StationContext.Provider>
  );
};

export default StationProvider;
