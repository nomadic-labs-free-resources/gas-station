export interface ITodo {
    id: number;
    title: string;
    description: string;
    status: boolean;
}

export type Notif = {
    type : string;
    message : string;
}

export type StationContextType = {
    pkh: string;
    connect: () => void;
    entryNames: string[];
    entryParams: ITypeDef[];
    btnLoading: boolean;
    jsonArea: string;
    notification: Notif;
    setNotification: (notif: Notif) => void;
    setJsonArea: (json: string) => void;
    getContractEntrypoints: (contractAddress: string) => void;
    getEntrypointsParams: (contractAddress: string, entrypoint: string) => void;
    test: () => void;
    sign: (contractAddress: string, entrypoint: string, jsonParameters: any) => void;
};

export type EntrypoinType = {
    name: string;
    jsonParameters: any;
    unused: boolean;
};

export interface Arg {
    key: string;
    value: string;
}

export interface ITypeDef {
    name: string;
    type: string;
    args: Arg[];
}

export type Entrypoint = {

}


export interface StationProviderProps {
    children: React.ReactNode
}