import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./styles/index.css";
import StationProvider from "./context/StationContext";

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <StationProvider>
      <App />
    </StationProvider>
  </React.StrictMode>
);
