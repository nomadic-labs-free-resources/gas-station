# Front-end

Here is the JSON to enter on the front. You have to put addresses in the from_ and to_. The address from_ must be the one you'll be signing with.

```shell
[ [ {
       "from_": "",
       "txs":
          [ {
             "to_": "",
             "token_id": "0",
             "amount": "100"
          } ]
} ] ]
```

To change the method of submission (two step or one step), you have to change the line 217 of the file "StationContext.tsx" located here: /frontend/src/context/
On this line you'll have to modify the address as follow:

- "http://127.0.0.1:3956/station/separateSend": if you want to do it in separate step

- "http://127.0.0.1:3956/station/oneSend": if you want to do it in one step
